from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
from textwrap import dedent
# from ..tasks.dataset_acoes import create_variables as create_acoes_variables
# from ..tasks.dataset_pessoas import create_variables as create_pessoas_variables


with DAG(
    'create_abt',
    default_args={
        'depends_on_past': False,
        'email': ['airflow@example.com'],
        'email_on_failure': False,
        'email_on_retry': False,
        'retries': 1,
        'retry_delay': timedelta(minutes=5),
        # 'queue': 'bash_queue',
        # 'pool': 'backfill',
        # 'priority_weight': 10,
        # 'end_date': datetime(2016, 1, 1),
        # 'wait_for_downstream': False,
        # 'sla': timedelta(hours=2),
        # 'execution_timeout': timedelta(seconds=300),
        # 'on_failure_callback': some_function,
        # 'on_success_callback': some_other_function,
        # 'on_retry_callback': another_function,
        # 'sla_miss_callback': yet_another_function,
        # 'trigger_rule': 'all_success'
    },
    description='A DAG that generates BigDataCorp variables',
    schedule_interval=timedelta(days=1),
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=['example'],
) as dag:

    dag.doc_md = __doc__  # providing that you have a docstring at the beginning of the DAG
    dag.doc_md = """
    This is a documentation placed anywhere
    """  # otherwise, type it like this
    templated_command = dedent(
        """
    {% for i in range(5) %}
        echo "{{ ds }}"
        echo "{{ macros.ds_add(ds, 7)}}"
    {% endfor %}
    """
    )

    # t1, t2 and t3 are examples of tasks created by instantiating operators
    t1 = BashOperator(
        task_id='read_dataset_acoes',
        bash_command='date',
    )

    t1.doc_md = dedent(
        """\
    #### Task Documentation
    You can document your task using the attributes `doc_md` (markdown),
    `doc` (plain text), `doc_rst`, `doc_json`, `doc_yaml` which gets
    rendered in the UI's Task Instance Details page.
    ![img](http://montcs.bloomu.edu/~bobmon/Semesters/2012-01/491/import%20soul.png)

    """
    )


    t2 = BashOperator(
        task_id='create_features_dataset_acoes',
        depends_on_past=False,
        bash_command='sleep 5',
        retries=3,
    )    

    t3 = BashOperator(
        task_id='create_features_dataset_pessoas',
        depends_on_past=False,
        bash_command=templated_command,
    )

    # t2 = PythonOperator(
    #     task_id='create_features_dataset_acoes',
    #     python_callable= create_acoes_variables,
    #     depends_on_past=False,
    #     dag=dag,
    #     retries=3,
    # )    

    # t3 = PythonOperator(
    #     task_id='create_features_dataset_pessoas',
    #     python_callable= create_pessoas_variables,
    #     depends_on_past=False,
    #     dag=dag,
    #     retries=3,
    # )        

    t1 >> [t2, t3]